% AMS595 Matlab Project #1-3
% Modify the AMS595_project_1_2.m into a function as pt_prob.m
% where function [total_points, pro] = pt_prob(precision)

precision = 4;  % define the level of precision

n = 10;  % (starting) total number of points

pt_even_pro = zeros(1,2);  % Craet a 1-by-2 array for assign (even # of points, probability)
pt_odd_pro = zeros(1,2);   % Craet a 1-by-2 array for assign (odd # of points, probability)


i = 1;  % order of random # in pt_array
k = 0;  % # of points lie outside the circle
diff_pro = 1;  % probability difference between pt_even_pro(2) and pt_odd_pro(2)

% Calculate the total number of points according to the level of precision
while (diff_pro ~= 0)
    
    % Calcualte how many points (k) that their distance to (0,0) are larger
    % than 1, the starting # of point is n.
    while (i <= n)
        pt_array = rand (2, n);   % Craet a 2-by-n array with random values, each colum means a (x,y) point.
        if (sqrt(pt_array (1, i)^2+pt_array (2, i)^2) > 1)  % if the distance between the point and (0,0) is larger than 1 
            k = k + 1;                                      % number of k increase by 1
            i = i + 1;                                      % number of i increase by 1
        else             % if the distance between the point and (0,0) is equal to or smaller than 1 
            i = i + 1;   % number of i increase by 1
        end
    end

    % Assign # of points and probability to pt_even_pro or pt_odd_pro
    if (mod(n,2) == 0)               % When the remainder of n/2 is equal to 0
        pt_even_pro(1) = n;          % Assign #(even) of points to pt_even_pro(1)
        pt_even_pro(2) = (n-k)/n;    % Assign probability to pt_even_pro(2)
    else                             % When the remainder of n/2 is not equal to 0
        pt_odd_pro(1) = n;           % Assign #(odd) of points to pt_odd_pro(1)
        pt_odd_pro(2) = (n-k)/n;     % Assign probability to pt_odd_pro(2
    end
    
    diff = pt_odd_pro(2)-pt_even_pro(2);                           % calculate the probability difference
    diff_pro = round (pt_odd_pro(2)-pt_even_pro(2), precision);    % round the difference to the level of precision
    
    
    if (diff_pro ~= 0)  % if the round probability difference is 0
        n = n + 1;      % move n to n+1
    else
        total_points = n;      % Done!!
    end
end


% Plot the n random points in and outside the circle
pt_incircle = zeros (2, n);
pt_outcircle = zeros (2, n);


for j = 1 : n
    if (sqrt(pt_array (1, j)^2+pt_array (2, j)^2) > 1)
        pt_outcircle (1, j) = pt_array (1, j);
        pt_outcircle (2, j) = pt_array (2, j);
    else
        pt_incircle (1, j) = pt_array (1, j);
        pt_incircle (2, j) = pt_array (2, j);
    end
end

% (n-k) will equal to the points that lie in the quarter of a circle.
pro = (n-k)/n ;
pi_cal = 4 * pro;

x_outcircle = pt_outcircle(1,:);
y_outcircle = pt_outcircle(2,:);
plot (x_outcircle, y_outcircle, '.', 'Markersize', 10)

hold on

x_incircle = pt_incircle(1,:);
y_incircle = pt_incircle(2,:);
plot (x_incircle, y_incircle, '.', 'Markersize', 10)

hold off



legend ('Outside','Inside', 'Location','southwest', 'fontsize', 16)
result = sprintf('Total points are %i ; Calculate pi is %f ; Precision is %i.', n, pi_cal, precision);
title (result, 'fontsize', 14)
axis('square')
