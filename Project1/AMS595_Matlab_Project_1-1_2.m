% AMS595 Matlab Project #1-1


a = 11;      % starting # of points
b = 100000;   % final # of points
c = b-a+1;   % # of data sets

% The below 4 arrays are built for plotting.
data_pt = zeros(1,c);     % creat a 1-D array to store the # of the points
data_pi = zeros(1, c);    % creat a 1-D array to store the calculated pi
data_time = zeros(1, c);  % creat a 1-D array to store the execution time
real_pi = zeros(1, c);    % creat a 1-D array to store the real pi value



for n = a : b
    
    tic;   % start to calculate the execution time

    %n = 100000  % Numbers of points

    pt_array = rand (2, n);   % Craet a 2-by-n array with random values


    % Calcualte the distance between the point and (0,0)
    for k = 1 : n

            % if the distance larger than 1, assign (0,0) to that point, which
            % means the point lies outside the circle; and else, assign (1,1)
            % to that point which means the point lies in the circle.
            if (sqrt(pt_array (1, k)^2+pt_array (2, k)^2) > 1)
                pt_array (1, k) = 0;
                pt_array (2, k) = 0;
            else
                pt_array (1, k) = 1;
                pt_array (2, k) = 1;
            end

    end


    sum_pt = sum(pt_array, 2);  % sum all the x values and y values

    cir_pts1 = sum_pt(1);  % Numbers of point in quarter of a circle for x
    cir_pts2 = sum_pt(2);  % Numbers of point in quarter of a circle for y
    % cir_pts1 should be equal to cir_pts2.

    % The probability of points in the quarter of a circle.
    Pro1 = cir_pts1/n ;
    Pro2 = cir_pts2/n ;
    format long, Pro1 ;
    format long, Pro2 ;
    % Pro1 should be equal to Pro2.

    pi_cal = 4 * Pro1 ;

    time = toc ;  % end of calculating the execution time

    p = n - 10;   % To assign the values into array, it needs to start from 1, because the starting # of points is 11. 
    data_pt(1,p) = n;               % To store the # point into array
    data_pi(1,p) = pi_cal ;         % To store the calculated pi into array
    data_time(1,p) = time ;         % To store the execution time into array. 
    real_pi(1,p) = pi() ;           % To store the real pi into array
   
end


subplot (2, 1, 1)
plot(data_pt, data_pi, '.', 'Markersize', 12)
xlabel('Number of points', 'fontsize', 20)
ylabel('pi values', 'fontsize', 20)
axis([a b 2.5 4])

hold on

subplot (2, 1, 1)
plot (data_pt, real_pi, 'linewidth', 3)
legend ('Calculated pi', 'Theoretical pi', 'fontsize', 16)

hold on


subplot (2, 1, 2)
plot(data_pt, data_time, 'm+', 'Markersize', 8)
xlabel('Number of points', 'fontsize', 20)
ylabel('time(s)', 'fontsize', 20)
axis([a b -0.00001 0.01])
legend ('Execution time', 'fontsize', 16)

hold off
