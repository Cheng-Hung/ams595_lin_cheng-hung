% AMS595 Matlab Project #1-1

tic;

n = 1000  % Numbers of points


pt_array = rand (2, n);   % Craet a 2-by-n array with random values


% Calcualte the distance between the point and (0,0)
for k = 1 : n
    
        % if the distance larger than 1, assign (0,0) to that point, which
        % means the point lies outside the circle; and else, assign (1,1)
        % to that point which means the point lies in the circle.
        if (sqrt(pt_array (1, k)^2+pt_array (2, k)^2) > 1)
            pt_array (1, k) = 0;
            pt_array (2, k) = 0;
        else
            pt_array (1, k) = 1;
            pt_array (2, k) = 1;
        end
   
end


sum_pt = sum(pt_array, 2);  % sum all the x values and y values

cir_pts1 = sum_pt(1);  % Numbers of point in quarter of a circle for x
cir_pts2 = sum_pt(2);  % Numbers of point in quarter of a circle for y
% cir_pts1 should be equal to cir_pts2.

% The probability of points in the quarter of a circle.
Pro1 = cir_pts1/n ;
Pro2 = cir_pts2/n ;
format long, Pro1 ;
format long, Pro2 ;
% Pro1 should be equal to Pro2.

pi_cal = 4 * Pro1

time = toc