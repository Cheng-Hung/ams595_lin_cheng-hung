% AMS595 Matlab Practice Problem #2 - 1

a_ij = zeros(10, 10)

  for i = 1:10
      for j = 1:10
          a_ij(i, j) = i^2 + j^2

      end
  end
  
  
% AMS595 Matlab Practice Problem #2 - 2
  
  a_ij = zeros(12, 12)

for i = 1:12
   for j = 1:12
       if (i == j)
          a_ij(i, j) = i*10+j
       elseif (i < j)
              a_ij(i, j) = i*10+j
       else
              a_ij(i, j) = 0
       end
   end
end



% AMS595 Matlab Practice Problem #2 - 3

a_ij = zeros(8, 8)

for i = 1:8
   for j = 1:8
       if (i == j)
          a_ij(i, j) = 0
       
       else
              a_ij(i, j) = (abs(i - j))^-1
       end
   end
   
end



% AMS595 Matlab Practice Problem #2 - 4

M = rand(3, 50)



% AMS595 Matlab Practice Problem #2 - 5

m_ij = rand(3, 50)

a_ij = m_ij.*m_ij

plot (a_ij, m_ij, 'k.')



% AMS595 Matlab Practice Problem #2 - 6

m_ij = rand(3, 50)

a_ij = zeros(3, 50)

for i = 1:3
    for j = 1:50
        if (m_ij(i, j) > 0.1)
            a_ij(i, j) = m_ij(i, j)
        else 
            a_ij(i, j) = 0
        end
    end
end

nom = sum(sum(abs(m_ij - a_ij)))/150



% AMS595 Matlab Practice Problem #2 - 7

x_i = 2*randi([1, 100], 1, 100)



% AMS595 Matlab Practice Problem #2 - 8

x_i = 2*randi([1, 100], 1, 100)

u_i = exp(x_i)

v_i = log(x_i)

w_i = sqrt((100-x_i).^2)



% AMS595 Matlab Practice Problem #2 - 9

x_i = 2*randi([1, 100], 1, 100)

u_i = exp(x_i)

v_i = log(x_i)

w_i = sqrt((100-x_i).^2)


plot (x_i, u_i, '.','MarkerSize',20)
hold on
plot (x_i, v_i, '*','MarkerSize',5)
hold on
plot (x_i, w_i, '+','MarkerSize',5)
hold off
axis([0 200 0 100])
legend('u=exp(x)', 'v=ln(x)', 'w=sqrt((100-x)^2)')