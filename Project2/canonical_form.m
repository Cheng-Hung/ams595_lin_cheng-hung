% AMS595 Matlab Project #2, 1-c - Gaussian Elimination
% Modify c. into a function as canonical_form.m

function [a_canonical, type] = canonical_form(a)

%p = 100;  % # of rows and columns
%a = randi ([-10,10],p);  % creat a p-by-p matrix with random integers ranging from -10 to 10
%a = [1 0 3 2; 2 0 9 0; 3 6 1 8; 0 3 0 1];
%a = [1 2 5 4; 5 7 7 8; 9 10 10 4; 3 1 2 5];

[m,n] = size(a);  % Get the size of the given matrix "a"

% To test input a_echelon is echelon form or not.
[form, type] = matrix_type(a);

% form == 2: echelon form
% form == 1: canonical form
% form == 3: neither


if (form == 2)  % If the input matrix is echelon form, do the backward substitution.

    % To generate a canonical form from chelon form
    % i: the sequence of row, ranging from 1 to m
    % j: the sequence of colimn, ranging from 1 to n

    a(m,:) = a(m,:)/a(m,n-1);
    %Convert the last pivot entry to 1. 

    i = m;
    for j = fliplr(1 : n-1)      %Flip the array.
        j;
        for k = fliplr(2 : j)  %Use the pivot to zero out the corresponding entry in all rows above it.
            k;
            a(k-1,:) = a(k-1,:)-a(i,:)*a(k-1,j);  
        end

        i = i-1; % Go to the above row.

        if (i > 0)   % This if loop is for stopping the conversion of the pivot to 1 as i<=0.
           a(i,:) = a(i,:)/a(i,i);  %Convert the pivot entry to 1.
        else    
        end

    end


    a_canonical = round (a, 6);
    [form, type] = matrix_type(a);
    % Round each entry in a to the 6th digits of decimal, otherwise for some
    % very samll number, it will not be consider as 0.

    
elseif (form == 1)    % If the input matrix is already canonical form, just return a to a_canonical.
       a_canonical = a;
       [form, type] = matrix_type(a);
       
       
else    % If neither neither echelon nor canonical form, nothing!
    a_canonical = a;
    type = sprintf('Input matrix is neither echelon nor canonical form. Please use echelon_form.m to do calculation first.');

                
end         
