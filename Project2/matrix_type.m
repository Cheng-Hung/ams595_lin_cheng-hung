% AMS595 Matlab Project #2, 1-a

% a. Write a Matlab function that will test whether a given matrix is in an 
%    echelon form, a row canonical form, or neither.

% Modify a. into a function as matrix_type.m

function [form, type] = matrix_type(a)

%p = 5;  % # of rows and columns

%a = randi ([0,1],p);  % creat a n-by-n matrix with random integers ranging from -10 to 10

%a = [1 0 0 0; 2 0 9 0; 3 6 1 8; 0 3 0 1];

%a = [1 2 3 4; 0 5 6 7; 9 0 8 9; 0 0 8 10];

[m,n] = size(a);  % Get the size of the given matrix "a"



% Test how many entries at the diagnoal are 1. (for canonical form)
i = 1;
j = 1;
k = 0; % calculate the # of entries which is 1.
k_not = 0; % calculaye the # of entries which is "not" 1.

while (i <= m)
   while (j <= n-1)   
        while (i == j)
            if (a(i,j) == 1)
                k = k+1;
                %sprintf ('a(%.i, %i) is 1', i, j)
            else
                k_not = k_not + 1;
                %disp('a is not row canonical form.')
            end
            j = j+1;
        end
        i = i+1;
   end
end





% Test how many entries off the diagnoal are 0. (for canonical form)
l = 0;  % calculaye the # of entries which is 0.
l_not = 0; % calculaye the # of entries which is "not" 0.

for i = 1 : m
    for j = 1 : n-1
        while (i ~= j)
            if (a(i,j) == 0)
               l = l+1;
               %sprintf ('a(%.i, %i) is 0', i, j)
            else
               l_not = l_not +1;
               %disp('a is not row canonical form.')
            end
            j = i;
        end
    end
end



% Test how many entries below the diagnoal are 0. (for echelon form)
p = 0; % calculaye the # of entries which is 0.
p_not = 0; % calculaye the # of entries which is "not" 0.

for i = 1 : m
    for j = 1 : n-1
        while (i > j)
            if (a(i,j) == 0)
               p = p+1;
               %sprintf ('a(%.i, %i) is 0', i, j)
            else
               p_not = p_not +1;
               %disp('a is not row canonical form.')
            end
            j = i;
        end
    end
end


%k
%l
%p

% if the # of "1" at the diagonal is same as the matrix size (n) and all of
% the entries off doagonal are zero, a is canonical.
if (k == m) && (l == m^2-m)  % the # of entries off diagonal in a n-by-n matrix is n^2-n
    form = 1;
    type = sprintf ('Matrix is row canonical form.');

% if the # of "1" at the diagonal is not equla to the matrix size (n) and 
% all of the entries below doagonal are zero, a is echelon.    
elseif (k ~= m) && (p == (m^2-m)/2)   % the # of entries below diagonal in a n-by-n matrix is (n^2-n)/2
    form = 2;
    type = sprintf ('Matrix is row echelon form.');

else
    form = 3;
    type = sprintf ('Matrix is neither echelon nor canonical form.');
        
end