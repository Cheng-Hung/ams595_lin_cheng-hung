% AMS595 Matlab Project #2, 2-a - Gaussian Elimination

function [type, a_inverse, upper_triangular, lower_triangular, permutation] = inverse_LU(a)

%p = 5;  % # of rows and columns
%a = randi ([-4,4],p);  % creat a p-by-p matrix with random integers ranging from -10 to 10

%a = [1 0 3 2; 2 0 9 0; 3 6 1 8; 0 3 0 1];
%a = [1 0 3 2; 3 6 1 8; 2 0 9 0; 0 3 0 1];
%a = [1 2 5 4; 5 7 7 8; 9 10 10 4; 3 1 2 5];
%a = [1 1 1; 8 -2 -1; 3 1 -2];

%a = "input_matrix.txt";

if (isnumeric(a) == 1)  % If input a is a matrix, do calculation directly.
    a = a;
elseif (isstring(a) == 1)  % If input a is a string, i.e., file name, read it by function dlmread().
    a = dlmread(a);        % e.g., a = "input_matrix.txt"
else
    disp ('Please input a matrix or a txt file with a matrix.')
end


[m,n] = size(a);  % Get the size of the given matrix "a"
a_in = a;   % Assign a to a_in to record the input matrix

P = eye (m);   % Creat a m-by-m identity matrix for tracking the product row-exchanged matrices.

M = eye (m);  % Creat a m-by-m identity matrix for tracking the product of forward elimination matrices.


% To generate chelon form from a given matrix.
% i: the sequence of row, ranging from 1 to m
% j: the sequence of colimn, ranging from 1 to n



if (m ~= n)
    type = sprintf ('Input matrix must be a square matrix.');
    a_inverse = "None";
    upper_triangular = a_inverse;
    lower_triangular = a_inverse;
    permutation = a_inverse;


else (m == n)
    
    i = 1;
    for j = 1 : n

        if (a(:,j) == 0)   % If column j is all 0, go to next column.
            sprintf ('Column %i is a free varaible.', j)

        elseif (a(i,j) == 0)
        l = i+1;   % Assign i+1 to l for swapping row_i and row_l

            while (l <= m)   % Before swapping, need to confirm a(l,j) is not eqal to 0.

                if (a(l,j) ~= 0)    % If a(l,j) is not eqal to 0
                   a([l i],:) = a([i l],:);   % Swap row_i and row_l in a
                   P_temp = eye(m);   % Creat a temporary m-by-m identity matrix, P_temp
                   P_temp([l i],:) = P_temp([i l],:);   % Swap row_i and row_l in P_temp to generate the row-exchanged elementary matrix.
                   M = P_temp*M;   % Apply the row-exchanged elementary matrix to M
                   P = P_temp*P;   % Apply the row-exchanged elementary matrix to P
                   l = m+1;   % Assign l to m+1 for breaking while loop.

                else     % If a(l,j) is eqal to 0, change to row_l+1.
                    l = l+1;
                end

            end

            for k = i+1 : m   
                M_temp = eye(m);  % Creat a temporary m-by-m identity matrix, M_temp
                M_temp(k,j) = -(a(k,j)/a(i,j));  % Assign the corresponding entry to M_temp to generate the elementary matrix.
                M = M_temp*M;   % Generate the product of all the elementary matrices.
                a(k,:) = a(k,:) - (a(k,j)/a(i,j))*a(i,:);  %  Use the pivot to zero out the corresponding entry in all rows below it.
            end



        else (a(i,j) ~= 0)
            for k = i+1 : m   
                M_temp = eye(m);  % Creat a temporary m-by-m identity matrix
                M_temp(k,j) = -(a(k,j)/a(i,j));  % Assign the corresponding entry to the temp to generate the elementary matrix.
                M = M_temp*M;   % Generate the product of all the elementary matrices.
                a(k,:) = a(k,:) - (a(k,j)/a(i,j))*a(i,:);  %  No swapping, use the pivot to zero out the corresponding entry in all rows below it.
            end

        end
        i = i+1;

    end


    
    permutation = P;
    
    
    upper_triangular = a;
    upper_triangular = round (a, 6);
    
    lower_triangular = P*M^-1;
    lower_triangular= round (lower_triangular, 6);
    
    forward_matrices = M;
    forward_matrices = round (M, 6);
    % P*a_in
    % L*U


    % To test input a_echelon is echelon form or not.
   

    B = eye (m);  % Creat a m-by-m identity matrix for tracking the product of back substitution matrices.

  
    % To generate a canonical form from chelon form
    % i: the sequence of row, ranging from 1 to m
    % j: the sequence of colimn, ranging from 1 to n

    B(m,:) = B(m,:)/a(m,n);
    a(m,:) = a(m,:)/a(m,n);
    %Convert the last pivot entry to 1. 

    i = m;
    for j = fliplr(1 : n)      %Flip the array.
        
        for k = fliplr(2 : j)  %Use the pivot to zero out the corresponding entry in all rows above it.
            
            B_temp = eye(m);  % Creat a temporary m-by-m identity matrix, B_temp
            B_temp(k-1,j) = -(a(k-1,j)/a(i,j));  % Assign the corresponding entry to B_temp to generate the elementary matrix.
            B = B_temp*B;   % Generate the product of all the elementary matrices.            
            a(k-1,:) = a(k-1,:)-a(i,:)*a(k-1,j); 
        end

        i = i-1; % Go to the above row.

        if (i > 0)   % This if loop is for stopping the conversion of the pivot to 1 as i<=0.
            B_temp = eye(m);  % Creat a temporary m-by-m identity matrix, B_temp
            B_temp(i,:) = B_temp(i,:)/a(i,i);  % Assign the corresponding entry (converting the pivot entry to 1) to B_temp to generate the elementary matrix.
            B = B_temp*B;   % Generate the product of all the elementary matrices.
            a(i,:) = a(i,:)/a(i,i);  %Convert the pivot entry to 1.
        else    
        end

    end
    
    a_canonical = a;
    a_canonical = round (a, 6);
    B = round (B, 6);

    for i = 1 : m
        
        % If all the entries in a row are all zeros, it means the input 
        % matrix is singular and the inverse matrix does not exist.
        if (sum (a_canonical(i, 1:n)) == 0)
            type = sprintf ('Error! Input matrix is singular.');
        else
            type = sprintf ('Input matrix has an inverse matrix. Good!');
            permutation
            upper_triangular
            lower_triangular
            back_matrices = B;
            a_inverse = back_matrices * forward_matrices
            
            % Svae the calculating matrices into text files.
            dlmwrite ('output_inverse.txt', a_inverse);
            dlmwrite ('output_upper.txt', upper_triangular);
            dlmwrite ('output_lower.txt', lower_triangular);
            dlmwrite ('output_P.txt', permutation);
            
        end
    end
    
end
