% AMS595 Matlab Project #2, 1-d - Gaussian Elimination


for p = 3:1000   % Matrix size from 3 to 1000

    tic;
    %p = 100;  % # of rows and columns
    A = randi ([-2,2], p);   % The given matrix A
    b = randi ([-2,2], p, 1);  % The given vector b
    [row_A,column_A] = size(A);    % The size of A, row_A shoueld be equal to coulumn_A.

    a = zeros (row_A,column_A+1);  % Create a row_A-by-column_A+1 matrix with 0 values to assign variables later.

    % Assign entries in A to a.
    for j = 1 : column_A
        for i = 1 : row_A
            a (i,j) = A (i,j);
        end
    end


    % Assign entries in b to a.
    for i = 1 : row_A
        a(i, column_A+1) = b (i, 1);
    end


    [a_echelon, type] = echelon_form(a);

    [a_canonical, type] = canonical_form(a_echelon);

    solution = zeros (row_A,1);   % Create a row_A-by-1 matrix with 0 values to assign solutions later.


    % Return to sol_type to seeif the matrix has the only solution, infinite 
    % solutions, or no solution.
    for  i = 1 : row_A 
                
        
        % If all the entries in one row are all zeros, it means there are 
        % infinite answers. (the # unknown > the # of known) 
        if (sum (a_canonical(i, 1:column_A)) == 0) && (a_canonical(i, column_A+1) == 0)
            sol_type = 1;

        % If all the all the entries except the last one in a row are zeros, it
        % means there is no solution for this matrix. (such as two parallel lines or planes)
        elseif (sum (a_canonical(i, 1:column_A)) == 0) && (a_canonical(i, column_A+1) ~= 0)
            sol_type = 2;

        else
            sol_type = 3;

        end
    end


    % Use sol_type to dispaly the final soluyion.
    if (sol_type == 1)    % sol_type == 1 means infinited solutions.
        disp ('Ax=b has infinited solutions.')

    elseif (sol_type == 2)    % sol_type == 2 means no solution.
        disp ('Ax=b has no solution.')

    else    % sol_type == 3 means only one solution and do assign the last column in a to solution.
        disp ('Ax=b has the olny one solution.');
        % Assign the last column in a_canonical to solution.
        for i = 1 : row_A
            solution (i, 1) = a_canonical(i, column_A+1);
        end

        solution;
    end


    time = toc;
    
    plot (p, time, '.', 'Markersize', 20)
    %legend ('Outside','Inside', 'Location','southwest', 'fontsize', 16)
    result = sprintf('Matrix size versus Calculation time');
    title (result, 'fontsize', 20)
    xlabel('Matrix size', 'fontsize', 20)
    ylabel('Time(s)', 'fontsize', 20)
    axis([0 p+5 -0.001 10])

    hold on
    

end
