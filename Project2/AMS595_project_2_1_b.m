% AMS595 Matlab Project #2, 1-b - Gaussian Elimination


p = 5;  % # of rows and columns


a = randi ([-2,2],p);  % creat a p-by-p matrix with random integers ranging from -10 to 10


P = eye (p);   % Creat a p-by-p identity matrix for tracking row-exchanged matrices.

M = eye (p);  % Creat a p-by-p identity matrix for forward elimination matrices.

%a = [1 0 3 2; 2 0 9 0; 3 6 1 8; 0 3 0 1];

%a = [1 2 5 4; 5 7 7 8; 9 10 10 4; 3 1 2 5];

[m,n] = size(a);  % Get the size of the given matrix "a"

% To generate chelon form from a given matrix.
% i: the sequence of row, ranging from 1 to m
% j: the sequence of colimn, ranging from 1 to n


i = 1;
for j = 1 : n
    
    if (a(:,j) == 0)   % If column j is all 0, go to next column.
        sprintf ('Column %i is a free varaible.', j)
    
    elseif (a(i,j) == 0)
    l = i+1;   % Assign i+1 to l for swapping row_i and row_l
        
        while (l <= m)   % Before swapping, need to confirm a(l,j) is not eqal to 0.

            if (a(l,j) ~= 0)    % If a(l,j) is not eqal to 0, swap row_i and row_l.
               a([l i],:) = a([i l],:);

            else     % If a(l,j) is eqal to 0, change to row_l+1.
                l = l+1;
            end
            l = m+1;   % Assign l to m+1 for breaking while loop.
        end
        
        for k = i+1 : m    %  After swapping, use the pivot to zero out the corresponding entry in all rows below i.
            a(k,:) = a(k,:) - (a(k,j)/a(i,j))*a(i,:);
        end


    else (a(i,j) ~= 0)
        for k = i+1 : m   %  No swapping, use the pivot to zero out the corresponding entry in all rows below it.
            a(k,:) = a(k,:) - (a(k,j)/a(i,j))*a(i,:);
        end
    
    end
    i = i+1;
    
end
  
a
